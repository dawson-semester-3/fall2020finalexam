package question3;

import java.util.Random;

public class CoinFlipGame {
	private Random rand = new Random();

	private int money = 100;

	public int getMoney() {
		return this.money;
	}

	/**
	 * @param  playerInput
	 * @param  bet         player's bet from a textfield
	 * @return             a message
	 */
	public String playRound(String playerInput, String bet) {
		if (!validateBet(playerInput, bet))
			return "Invalid bet input";

		String coinFace = rand.nextBoolean() ? "heads" : "tails";

		if (playerInput.equals(coinFace)) {
			money += Integer.parseInt(bet);
			return "You won " + bet + "!";
		}
		else {
			money -= Integer.parseInt(bet);
			if (money == 0) {
				return "You're out of money!!!";
			}
		}
		return "You lost " + bet + " :(";
	}

	private boolean validateBet(String playerInput, String bet) {
		int betCount = 0;
		try {
			betCount = Integer.parseInt(bet);
		} catch (NumberFormatException e) {
			return false;
		}

		if (getMoney() < betCount)
			return false;
		return true;
	}
}

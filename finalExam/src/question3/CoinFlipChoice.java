package question3;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class CoinFlipChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField money;
	private TextField bet;
	private String playerChoice;
	private CoinFlipGame coinFlipGame;

	public CoinFlipChoice(TextField message, TextField money, TextField bet, String playerChoice, CoinFlipGame coinFlipGame) {
		this.message = message;
		this.money = money;
		this.bet = bet;
		this.playerChoice = playerChoice;
		this.coinFlipGame = coinFlipGame;
	}

	@Override
	public void handle(ActionEvent event) {
		message.setText(coinFlipGame.playRound(playerChoice, bet.getText())); // plays a round then sets message's text
		money.setText("Money: " + coinFlipGame.getMoney());
	}
}

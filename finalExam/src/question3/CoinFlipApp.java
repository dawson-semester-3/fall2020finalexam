package question3;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class CoinFlipApp extends Application {
	private CoinFlipGame coinFlipGame = new CoinFlipGame();

	@Override
	public void start(Stage stage) {
		stage.setTitle("Rock Paper Scissors by Neil Fisher");

		Group root = new Group();

		Scene scene = new Scene(root, 400, 300);
		scene.setFill(Color.BLACK);
		// creating nodes
		VBox Vnodes = new VBox();
		// buttons
		HBox coinChoice = new HBox();
		Button heads = new Button("heads");
		Button tails = new Button("tails");
		coinChoice.getChildren().addAll(heads, tails);
		// textfields
		TextField message = new TextField("Click on heads or tails");
		message.setEditable(false);

		Text betText = new Text("Place Bet: ");
		betText.setFill(Color.WHITE);
		TextField bet = new TextField();
		bet.setPrefWidth(scene.getWidth() / 4);

		TextField money = new TextField("Money: 100");
		money.setPrefWidth(scene.getWidth() / 4);
		money.setEditable(false);

		Vnodes.getChildren().addAll(money, new HBox(betText, bet), coinChoice, new HBox(new Text("-----------------------")), message);

		// event listeners
		heads.setOnAction(new CoinFlipChoice(message, money, bet, "heads", coinFlipGame));
		tails.setOnAction(new CoinFlipChoice(message, money, bet, "tails", coinFlipGame));

		root.getChildren().add(Vnodes);
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}

package question2;

public class Recursion {
	public static int recursiveCount(int[] numbers, int n) {
		int count = 0;
		if (numbers.length > n) {
			if (numbers[n] < 10 && numbers[n] % 2 == 0 && numbers[n] >= n) {
				count = recursiveCount(numbers, n + 1) + 1;
				return count + 1;
			}
			count = recursiveCount(numbers, n + 1);
		}
		return count;
	}

	public static void main(String[] args) {
		int n = 6;
		System.out.println(recursiveCount(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, n));
	}
}